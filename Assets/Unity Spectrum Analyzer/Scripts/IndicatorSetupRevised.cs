﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MicAudioAnalyzer))]
public class IndicatorSetupRevised : MonoBehaviour {

	public GameObject elementPrefab;

	void Start()
	{
		int bands = GetComponent<MicAudioAnalyzer>().bandLevels.Length;
		for (int i = 0; i < bands; i++)
		{
			var element = Instantiate(elementPrefab);
			element.GetComponent<IndicatorElementRevised>().index = i;
			element.transform.parent = transform;
			element.transform.localPosition = Vector3.right * i;
		}
	}
}
