﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicAudioAnalyzer : MonoBehaviour {

	public int spectrumSamples = 1024;
	public float interval = 0.02f;
	public int numberOfBands = 8;

	float[] rawSpectrum;

	[HideInInspector]
	public float[] bandLevels;

	void Awake ()
	{
		rawSpectrum = new float[spectrumSamples];
		bandLevels = new float[numberOfBands];
	}

	IEnumerator Start()
	{
		WaitForSeconds wait = new WaitForSeconds(interval);
		while (true)
		{
//			Microphone.
			AudioListener.GetSpectrumData(rawSpectrum, 0, FFTWindow.BlackmanHarris);
			ConvertRawSpectrumToBandLevels();

			yield return wait;
		}
	}
	
	void ConvertRawSpectrumToBandLevels()
	{
		float coeff = Mathf.Log(rawSpectrum.Length);
		var offs = 0;

		for (int i = 0; i < bandLevels.Length; i++)
		{
			var next = Mathf.Exp(coeff / bandLevels.Length * (i + 1));
			float weight = 1.0f / (next - offs);

			for (float sum = 0.0f; offs < next; offs++)
			{
				sum += rawSpectrum[offs];
				bandLevels[i] = Mathf.Sqrt(weight * sum);
			}
		}
	}

	void OnAudioFilterRead(float[] data, int channels)
	{
//		for (var i = 0; i < data.Length; i += channels)
//		{
//			var level = data[i];
//			squareSum += level * level;
//		}
//
//		sampleCount += data.Length / channels;
//
//		if (mute)
			for (var i = 0; i < data.Length; i++)
				data[i] = 0;
	}
}
