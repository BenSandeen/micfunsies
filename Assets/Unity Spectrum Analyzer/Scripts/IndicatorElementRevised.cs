﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorElementRevised : MonoBehaviour {

	public int index = 0;
	float meter = 0.0f;
	public MicAudioAnalyzer analyzer;

	void Start()
	{
		if (analyzer == null)
			analyzer = FindObjectOfType<MicAudioAnalyzer>();
	}

	void Update ()
	{
		meter = Mathf.Max(meter * Mathf.Exp(-10.0f * Time.deltaTime), analyzer.bandLevels[index]);
		transform.localScale = transform.localScale.SetY(meter * 40.0f);
//		transform.localScale.y = meter * 40.0;
	}
}
