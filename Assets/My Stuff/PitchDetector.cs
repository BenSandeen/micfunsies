﻿//http://www.kaappine.fi/tutorials/fundamental-frequencies-and-detecting-notes/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PitchDetector : MonoBehaviour {

	[AutoFind]
	public AudioSource audSource;

	[Range(0f, 0.1f)]
	public float volumeThreshold;
	public float detectedPitch;
	float detectedVolume;
	public float interval = 0.2f;
	public int sampleRate = 1024;

	public bool aboveThreshold;

    // Variables for Ben's implementation (see: http://answers.unity3d.com/questions/157940/getoutputdata-and-getspectrumdata-they-represent-t.html)
    //////////////////////////////////////////////////////////////////////////////////////
    int qSamples = 1024;  // array size
    float refValue = 0.1f; // RMS value for 0 dB
    float threshold = 0.02f;      // minimum amplitude to extract pitch
    float rmsValue;   // sound level - RMS
    float dbValue;    // sound level - dB
    float pitchValue; // sound pitch - Hz
 
    private float[] samples; // audio samples
    private float[] spectrum; // audio spectrum
    private float fSample;

//    private Hashtable freqsToNotes = GetFreqsToNotesMapping();
    //////////////////////////////////////////////////////////////////////////////////////

    //	public DywapitchWrapper dywa;

    void Update()
	{
//		detectedPitch = GetFundamentalFrequency();
		detectedPitch = GetFrequency();
		detectedVolume = GetAveragedVolume();
//		SendDataForAnalysis();

		aboveThreshold = detectedVolume > volumeThreshold;
	}

//	void SendDataForAnalysis()
//	{
//		float[] data = new float[8192];
//		audSource.GetSpectrumData(data,0,FFTWindow.BlackmanHarris);
//		dywa.ComputePitch(data, 0, 8192);
//	}

	float GetFundamentalFrequency()
	{
		float fundamentalFrequency = 0.0f;
		float[] data = new float[8192];
		audSource.GetSpectrumData(data,0,FFTWindow.BlackmanHarris);
		float s = 0.0f;
		int i = 0;
		for (int j = 1; j < 8192; j++)
		{
			if ( s < data[j] )
			{
				s = data[j];
				i = j;
			}
		}

		fundamentalFrequency = i * sampleRate / 8192;
		return fundamentalFrequency;
	}

	float GetAveragedVolume()
	{ 
		float[] data = new float[256];
		float a = 0;
		audSource.GetOutputData(data,0);
		foreach(float s in data)
		{
			a += Mathf.Abs(s);
		}

		return a/256;
	}

	void InitializeSpectrumArray()
	{
		if (spectrum == null)
			spectrum = new float[8192];
	}

    float GetFrequency()
    {
		InitializeSpectrumArray();
		audSource.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);
        float maxIntensity = 0f;
        int maxIntensityIndex = 0;

        for (int i = 0; i < qSamples; i++)   // find max
        {
            if (spectrum[i] > maxIntensity && spectrum[i] > threshold)
            {
                maxIntensity = spectrum[i];
                maxIntensityIndex = i;  // maxIntensityIndex is the index of max
            }
        }

        double freqIndex = maxIntensityIndex;  // pass the index to a float variable
        if (maxIntensityIndex > 0 && maxIntensityIndex < qSamples - 1)  // interpolate index using neighbours
        {
            float dL = spectrum[maxIntensityIndex - 1] / spectrum[maxIntensityIndex];
            float dR = spectrum[maxIntensityIndex + 1] / spectrum[maxIntensityIndex];
            freqIndex += 0.5 * (dR * dR - dL * dL);
        }

        pitchValue = (float)(freqIndex * (fSample / 2) / qSamples);
		return pitchValue;
    }

//    private static Hashtable GetFreqsToNotesMapping()
//    {
//        float middle_A = 440.0f;
//        Hashtable freqsToNotes = new Hashtable();
//        freqsToNotes.Add(middle_A, "A_4");
//
//        for (int i = 4; i >= 0; i--)
//        {
//            // Fill in `freqsToNotes` here
//        }
//    }

}
