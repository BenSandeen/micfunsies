﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PitchToPosition : MonoBehaviour {

	public PitchDetector pitchDetector;
//	public DywapitchWrapper dywa;
	public bool resetOnSilence;

	[Range(0,1)]
	public float sensitivity = 0.1f;
	Vector3 initialPos;

	void Awake()
	{
		initialPos = transform.localPosition;
	}

	void Update()
	{
		if (pitchDetector.aboveThreshold)
			Move(pitchDetector.detectedPitch);
//			Move((float)dywa.computedPitch);

		else if (resetOnSilence)
			Reset();
	}

	void Move(float pitch)
	{
		transform.localPosition = initialPos + Vector3.up * pitch * sensitivity;
	}

	void Reset()
	{
		transform.localPosition = initialPos;
	}

}
