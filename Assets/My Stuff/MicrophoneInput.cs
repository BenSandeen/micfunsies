﻿//http://answers.unity3d.com/questions/1113690/microphone-input-in-unity-5x.html
using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class MicrophoneInput : MonoBehaviour {

	[AutoFind]
	public AudioSource audSource;
	public bool startMicOnStartup = true;

	bool microphoneListenerOn = false;
	float timeSinceRestart = 0;

	void Start()
	{        
		if (startMicOnStartup)
		{
			RestartMicrophoneListener();
			StartMicrophoneListener();
		}
	}

	void Update()
	{   
		MicrophoneIntoAudioSource (microphoneListenerOn);
	}

	[EasyButtons.Button]
	public void StopMicrophoneListener()
	{
		microphoneListenerOn = false;
		audSource.Stop ();
		audSource.clip = null;

		Microphone.End (null);
	}

	[EasyButtons.Button]
	public void StartMicrophoneListener()
	{
		microphoneListenerOn = true;
		RestartMicrophoneListener ();
	}
		
	public void RestartMicrophoneListener()
	{
		audSource.clip = null;
		timeSinceRestart = Time.time;
	}

	void MicrophoneIntoAudioSource (bool MicrophoneListenerOn)
	{
		if(MicrophoneListenerOn)
		{
			//pause a little before setting clip to avoid lag and bugginess
			if (Time.time - timeSinceRestart > 0.5f && !Microphone.IsRecording (null)) {
				audSource.clip = Microphone.Start (null, true, 10, 44100);

				//wait until microphone position is found (?)
				while (!(Microphone.GetPosition (null) > 0)){}

				audSource.Play ();
			}
		}
	}

}