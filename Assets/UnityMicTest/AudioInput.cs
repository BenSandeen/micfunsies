﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class AudioInput : MonoBehaviour
{
	AudioSource audioS;

	void Awake()
	{
		audioS = GetComponent<AudioSource>();
	}

	void Start()
    {
        audioS.Stop();

        audioS.loop = true;

        int minFreq, maxFreq;
        Microphone.GetDeviceCaps(null, out minFreq, out maxFreq);

        audioS.clip = Microphone.Start(null, true, 1, maxFreq > 0 ? maxFreq : 44100);

        while (audioS.clip != null)
        {
            int delay = Microphone.GetPosition(null);
            if (delay > 0)
            {
                audioS.Play();
                Debug.Log("Latency = " + (1000.0f / audioS.clip.frequency * delay) + " msec");
                break;
            }
        }
    }

    void OnApplicationPause(bool paused)
    {
        if (paused)
        {
            audioS.Stop();
            Microphone.End(null);
            Debug.Log("paused");
        }
        else
        {
            Start();
        }
    }
}
