﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class DywapitchWrapper : MonoBehaviour {

	[StructLayout(LayoutKind.Sequential)]  
	public struct dywapitchtracker {  
		double	_prevPitch;
		int		_pitchConfidence;
	} 

	dywapitchtracker tracker;
	double[] samplesies;
//	double[] sa
	public int numSamples = 8192;
	public double computedPitch;
	bool initialized;

	[DllImport ("dywapitch")]
	private static extern int dywapitch_neededsamplecount(int minFreq);
	//int dywapitch_neededsamplecount(int minFreq);

	[DllImport ("dywapitch")]
	private static extern void dywapitch_inittracking(ref dywapitchtracker pitchtracker);
	//void dywapitch_inittracking(dywapitchtracker *pitchtracker);

	[DllImport ("dywapitch")]
	private static extern double dywapitch_computepitch(ref dywapitchtracker pitchtracker, ref double samples, int startsample, int samplecount);
	//double dywapitch_computepitch(dywapitchtracker *pitchtracker, double * samples, int startsample, int samplecount);


	void Start()
	{
		InitializeTracker();
		samplesies = new double[numSamples];
		initialized = true;
	}

//	void Update()
//	{
//		if (initialized)
//		{
//			computedPitch = ComputePitch();
//		}
//	}

	public void InitializeTracker()
	{
		Debug.Log("initializing dywapitch");
		tracker = new dywapitchtracker();
		dywapitch_inittracking(ref tracker);
	}

	public double ComputePitch(float[] sampleArray, int start, int count)
	{
		FloatToDouble(ref samplesies, sampleArray);
		double result = dywapitch_computepitch(ref tracker, ref samplesies[0], start, count);
//		Debug.Log(result);
		return result;
	}

	void FloatToDouble(ref double[] output, float[] input)
	{
		if (input == null)
			return;

		if (output.Length != input.Length)
		{
			Debug.LogError("poop");
			return;
		}

		int n = input.Length;

		for (int i = 0; i < n; i++)
		{
			output[i] = (double)input[i];
		}
	}
}
