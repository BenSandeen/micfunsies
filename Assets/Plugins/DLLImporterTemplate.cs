﻿#if false //THIS IS A TEMPLATE. DO NOT USE FOR REALSIES

using UnityEngine;
using System.Runtime.InteropServices;

public class DLLImporterTemplate : MonoBehaviour {

	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	#else
	//DON'T NEED EXTENSION, JUST NAME OF BUNDLE FILE (NOT CLASS)
	[DllImport ("test")] //NEEDS TO BE ON EVERY FUNCTION
	#endif

	private static extern int ImportedFunctionName (); //IMPORTED FUNCTION
}

#endif