﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PitchPrinter : MonoBehaviour {
	
	public PitchDetector pitchDetector;

	[AutoFind]
	public Text text;
	
	// Update is called once per frame
	void Update () {
		text.text = pitchDetector.detectedPitch.ToString();
	}
}
