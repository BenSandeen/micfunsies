﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Screenshot {

	[MenuItem("Screenshot/Take Screenshot")]
	public static void TakeScreenshot()
	{
		Application.CaptureScreenshot("/Assets/screenshot.png");
		Debug.Log("screenshot");
	}

}
