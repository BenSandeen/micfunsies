﻿using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Diagnostics;
using System.IO;
using Debug = UnityEngine.Debug;
using UnityEngine.Assertions;

public class AutomateBuild 
{
	static BuildPlayerOptions options;
	static string[] levels;
	static bool steam;

	static AutomateBuild()
	{
		options = new BuildPlayerOptions();
		levels = new string[] {"Assets/Scenes/Splash.unity", "Assets/Scenes/Menu.unity", "Assets/Scenes/LevelBase.unity"};
	}

	[MenuItem("Builds/Non-Steam Builds (not fully implemented yet)")]
	public static void BuildNonSteam ()
	{
		steam = false;
		BuildPlayers("NOACHIEVEMENTS;DISABLESTEAMWORKS");
	}

	[MenuItem("Builds/Steam Builds (not fully implemented yet)")]
	public static void BuildSteam ()
	{
		steam = true;
		BuildPlayers("");
	}

	[MenuItem("Builds/Switch to non-steam mode")]
	public static void Ugh ()
	{
		PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, "DISABLESTEAMWORKS");
	}

	static void BuildPlayers(string customDefineSymbols)
	{
		// Get common folder for all builds
		string commonPath = EditorUtility.SaveFolderPanel("Choose common location of all builds", "~/Documents/Game Development/Builds", "");
		if (commonPath.Length == 0)
			return;
		
		//Set custom define symbols
		PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, customDefineSymbols);
		
		//Build the different players
		Build(BuildTarget.StandaloneOSXIntel64, commonPath);
//		Build(BuildTarget.StandaloneOSXUniversal, commonPath);
		Build(BuildTarget.StandaloneWindows64, commonPath);
//		Build(BuildTarget.StandaloneWindows, commonPath);
//		Build(BuildTarget.StandaloneLinuxUniversal, commonPath);

		//Log date/time of build
		File.WriteAllText(commonPath + "/info.txt", "These builds were created at " + System.DateTime.Now.ToString() + "\nWith symbols: " + customDefineSymbols + "\n(' v ' )");
		
	}

	static void Build(BuildTarget target, string commonPath)
	{
		options.target = target;
		options.locationPathName = commonPath + ChooseEndPath(target);
		options.scenes = levels;
		options.options = BuildOptions.ShowBuiltPlayer;
		BuildPipeline.BuildPlayer(options);

		//Copy any readmes, etc. if needed
		CopyStuff();

		if (!steam)
			DeleteSteamDlls(options.locationPathName);

		Debug.Log("built for " + target.ToString());
	}

	static string ChooseEndPath(BuildTarget target)
	{
		switch (target)
		{
			case BuildTarget.StandaloneOSXUniversal:
				return "/Adjacency_Mac_Universal/Adjacency";

			case BuildTarget.StandaloneOSXIntel64:
				return "/Adjacency_Mac64bit/Adjacency";

			case BuildTarget.StandaloneOSXIntel:
				return "/Adjacency_Mac32bit/Adjacency";

			case BuildTarget.StandaloneWindows:
				return "/Adjacency_Win32bit/Adjacency";

			case BuildTarget.StandaloneWindows64:
				return "/Adjacency_Win64bit/Adjacency";

			case BuildTarget.StandaloneLinuxUniversal:
				return "/Adjacency_Linux/Adjacency";

			default:
				return "/whoops/Whoopsy";
		}
	}

	static void DeleteSteamDlls(string pathOfBuild)
	{
//		#if DISABLESTEAMWORKS

		string searchPath = Directory.GetParent(pathOfBuild).ToString();
		string searchTerm = "*steam*";
		string searchTerm2 = "*Steam*";

		string[] files = Directory.GetFiles(searchPath, searchTerm, SearchOption.AllDirectories);
		ArrayUtility.AddRange(ref files, Directory.GetFiles(searchPath, searchTerm2, SearchOption.AllDirectories));
		ArrayUtility.AddRange(ref files, Directory.GetDirectories(searchPath, searchTerm2, SearchOption.AllDirectories)); //to catch CSteamworks.bundle

		foreach (string file in files)
			Debug.Log("deleting " + file);

		foreach (string file in files)
			FileUtil.DeleteFileOrDirectory(file);

//		#endif
	}

	static void CopyStuff()
	{
//		// Copy a file from the project folder to the build folder, alongside the built game.
//		FileUtil.CopyFileOrDirectory("Assets/Templates/Readme.txt", path + "Readme.txt");
	}

}