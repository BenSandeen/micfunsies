using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;

public static class CustomInspectorCreator
{
    [MenuItem("Assets/Create/Custom Inspector", priority = 81)]
    static void CreateInsptorEditorClass()
    {
        Object obj = Selection.objects[0];
        string assetPath = AssetDatabase.GetAssetPath(obj);

        var filename = Path.GetFileNameWithoutExtension(assetPath);
        var script = string.Format(template, filename);
        var editorFolder = Path.GetDirectoryName(assetPath) + "/Editor";

        if (!Directory.Exists(editorFolder))
        {
            Directory.CreateDirectory(editorFolder);
        }

        File.WriteAllText(editorFolder + "/" + filename + "Inspector.cs", script);
        AssetDatabase.Refresh();
    }

    [MenuItem("Assets/Create/Custom Inspector", priority = 81, validate = true)]
    static bool ValidateCreateInsptorEditorClass()
    {
        Object obj = Selection.objects[0];
        string path = AssetDatabase.GetAssetPath(obj);

        if (!path.EndsWith(".cs"))
            return false;
        if (path.Contains("Editor"))
            return false;


        return true;
    }

    static string template = @"using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof({0}))]
//[CanEditMultipleObjects]
public class {0}Inspector : Editor
{{
    void OnEnable()
    {{
        // TODO: find properties we want to work with
        //serializedObject.FindProperty();
    }}

    public override void OnInspectorGUI()
    {{
        // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
        serializedObject.Update();

        // TODO: Draw UI here
        //EditorGUILayout.PropertyField();
        DrawDefaultInspector();

        // Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
        serializedObject.ApplyModifiedProperties();
    }}
}}
";
}
