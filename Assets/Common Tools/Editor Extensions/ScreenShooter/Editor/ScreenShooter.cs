using UnityEditor;
using UnityEngine;

/*
 * Screen Shooter: App Store screenshot automation
 * Multi-sized screenshots with one click!
 * 
 * (Unity Pro required)
 * 
 * Instructions: 
 * 
 * - Place 'ScreenShooter.cs' in your Editor directory
 * - Select 'Tools > Screen Shooter' to open the tool
 * - Hit 'Play' and start grabbing game screens
 * 
 * Free to distribute and use at your own risk!
 * 
 * Comments or suggestions: support@ezone.com
 * 
 * ScreenShooter
 * 
 * v1.2.3 update Oct 25, 2015
 * -iPad Pro screen size 2732x2048
 * 
 * v1.2.2 update Oct 22, 2015
 * - tvOS screen sizes 1920x1080
 * 
 * v1.2.1 update Oct 10, 2014
 * - auto-shot if lots of particle systems
 * - updated iPhone6+ screen size
 * 
 * v1.2 update Sept 10, 2014
 * - iPhone 6 and 6+ screen sizes
 * 
 * v1.1 update August 8, 2014
 * - auto-updates file numbers
 * - Larger iPad size
 * - Print size
 * - Cmd-G key to start screenshots
 * - Places in 'ScreenShooter' directory
 * 
 * v1.0 Feb 14, 2014
 * 
 * - first build
 */

public class ScreenShooter : EditorWindow {

	string fileName = "Screen";
	string captureButton = "Start Shootin'! <Cmd-G>";
	string captureDrawCallButton = "Start Particle Capture";
	string state = "Ready";
	int screenID = 0;
	float changeTime = 0;
	static Rect[] screens;
	string[] screenNames;
	float pauseTime = 0.01f;
	float origTimeScale;
	bool landscape = true;
	bool portrait = true;
	bool printSize = false;
	bool extraTimeDelay = false;
	Rect origRect;
	int id = 0;
	int pixelDensity = 1;

	int drawCalls = 0;
	int baseDrawCalls = 5;
	ParticleSystem[] allObjects;
	int drawCheckCounter = 0;
	bool multipleShots = true;
	bool autoCapture = false;

	[MenuItem("Tools/Screen Shooter 1.2.3")] 
	static void Init () {
		ScreenShooter window = 
			(ScreenShooter)EditorWindow.GetWindow(typeof(ScreenShooter));
		if (window == null) Debug.Log("No window");
		System.IO.Directory.CreateDirectory(Application.dataPath + "/../ScreenShooter");
	}


	void OnGUI () {
		EditorGUILayout.LabelField (" ");
		EditorGUILayout.LabelField ("Screen Shooter: App Store screenshot automation");
		EditorGUILayout.LabelField ("Multi-sized screenshots with one click!");
		EditorGUILayout.LabelField (" ");

		id = EditorGUILayout.IntField ("Shot #:", id);

		landscape = EditorGUILayout.Toggle("Landscape", landscape);
		portrait = EditorGUILayout.Toggle("Portrait", portrait);
		printSize = EditorGUILayout.Toggle("Print Size Version", printSize);
		extraTimeDelay = EditorGUILayout.Toggle("Extra Time Delay", extraTimeDelay);

		if(GUILayout.Button(captureButton)) {
			if(state == "Ready") {			
				StartCapture();
			}
		}


		EditorGUILayout.HelpBox("Notes:\n" +
		    "1. Set Game window to 'Free Aspect'\n" +
		    "3. Game window gets undocked (sorry) no way around it :(\n" +
			"4. Screenshots are saved in your Project Directory in 'ScreenShooter'.\n" +
			"5. Want more sizes? Edit 'ScreenShooter.cs'!\n", MessageType.None);


		baseDrawCalls = EditorGUILayout.IntField ("Particle Threshold:", baseDrawCalls);
		multipleShots = EditorGUILayout.Toggle("Multiple Shots:", multipleShots);

		if(GUILayout.Button(captureDrawCallButton)) {
			if(state == "Ready") {
				StartDrawCheck();
			} else if (state == "CheckDrawCalls") {
				state = "Ready";
				autoCapture = false;
				captureDrawCallButton = "Start Particle Capture";
			}
		}

		EditorGUILayout.LabelField ("Current Particle Count: " + drawCalls);

	}


	void StartDrawCheck() {
		autoCapture = true;
		state = "CheckDrawCalls";
		drawCheckCounter = 100;
		CheckDrawCalls();
		captureDrawCallButton = "Waiting for particle count above " + baseDrawCalls + " (click again to cancel)";
	}

	void CheckDrawCalls() {
		drawCheckCounter++;
		if (drawCheckCounter < 10) return;
		drawCheckCounter = 0;
		drawCalls = 0;
		allObjects = (ParticleSystem[])ParticleSystem.FindObjectsOfType(typeof(ParticleSystem)) ;

		drawCalls = allObjects.Length;
		if ( drawCalls > baseDrawCalls) StartCapture();
		Repaint();
	}
	

	void StartCapture() {
		captureButton = "Wait...";	
		captureDrawCallButton = "Wait...";

		if (extraTimeDelay) {
			pauseTime = 0.5f;
		} else {
			pauseTime = 0.01f;
		}

		origTimeScale = Time.timeScale;
		Time.timeScale = 0f;

		origRect = GetMainGameView().position;
		screens = new Rect[17];
		screenNames = new string[17];
		// Portrait
		int tempID = 0;
		screens[tempID] = new Rect(10,100,640,960); // iOS 3.5 inch retina
		screenNames[tempID] = "3-5_Port_";
		tempID++;
		screens[tempID] = new Rect(10,100,640,1136); // iOS 4 inch retina
		screenNames[tempID] = "4-0_Port_";
		tempID++;
		screens[tempID] = new Rect(10,100,750,1334); // iOS 4.7 inch retina 1334 x 750
		screenNames[tempID] = "4-7_Port_";
		tempID++;
		screens[tempID] = new Rect(10,100,621,1104); // iOS 5.5 inch retina 1242 x 2208 - double it! 
		screenNames[tempID] = "5-5_Port_";
		tempID++;
		screens[tempID] = new Rect(10,100,768,1024); // iOS iPad - double it!
		screenNames[tempID] = "iPad_Port_";
		tempID++;
		screens[tempID] = new Rect(10,100,1024,1366); // iPad Pro (double it - 2732x2048)
		screenNames[tempID] = "iPadPro_Port_";
		tempID++;
		screens[tempID] = new Rect(10,100,800,1280); // Google Play
		screenNames[tempID] = "Android_Port_";
		tempID++;
		screens[tempID] = new Rect(10,100,800,1280); // Print
		screenNames[tempID] = "Print_Port_";

		// Landscape
		tempID++;
		screens[tempID] = new Rect(10,100,960,640); // iOS 3.5 inch retina
		screenNames[tempID] = "3-5_Land_";
		tempID++;
		screens[tempID] = new Rect(10,100,1136,640); // iOS 4 inch retina
		screenNames[tempID] = "4-0_Land_";
		tempID++;
		screens[tempID] = new Rect(10,100,1334,750); // iOS 4.7 inch retina 1334 x 750
		screenNames[tempID] = "4-7_Land_";
		tempID++;
		screens[tempID] = new Rect(10,100,1104,621); // iOS 5.5 inch retina 1242 x 2208 - double it!
		screenNames[tempID] = "5-5_Land_";
		tempID++;
		screens[tempID] = new Rect(10,100,1024,768); // iOS iPad
		screenNames[tempID] = "iPad_Land_";
		tempID++;
		screens[tempID] = new Rect(10,100,1280,800); // Google Play
		screenNames[tempID] = "Android_Land_";
		tempID++;
		screens[tempID] = new Rect(10,100,1920,1080); // tvOS
		screenNames[tempID] = "tvOS_Land_";
		tempID++;
		screens[tempID] = new Rect(10,100,1366,1024); // iPad Pro (double it - 2732x2048)
		screenNames[tempID] = "iPadPro_Land_";
		tempID++;
		screens[tempID] = new Rect(10,100,1280,800); // Print (pixel double)
		screenNames[tempID] = "Print_Land_";

		state = "Resize";
		screenID = 0;
	}


	void FinishCapture() {
		Time.timeScale = origTimeScale;
		state = "Ready";
		captureButton = "Start Shootin'! <Cmd-G>";

		id++;
		GetMainGameView().position = origRect;
		Repaint();

		if (autoCapture && multipleShots) {
			// get ready for more particle system shots
			changeTime = Time.realtimeSinceStartup + 2.0f; // 2 second gap between auto captures
			state = "AutoCapturePause";
			captureDrawCallButton = "Waiting for particle count above " + baseDrawCalls + " (click again to cancel)";
		} else {
			captureDrawCallButton = "Start Particle Capture";
		}
	}

	void NextScreen() {
		screenID++;
		if (screenID < screens.Length) {
			state = "Resize";
		} else {
			FinishCapture();
		}
	}


	void Update() {
		switch (state) {
		case "AutoCapturePause":
			if (Time.realtimeSinceStartup > changeTime) {
				if (multipleShots) {
					StartDrawCheck();
				} else {
					state = "Ready";
					captureDrawCallButton = "Start Particle Capture";
				}
			}
			break;
		case "Resize":
			bool skip = false;
			Rect tempRect = screens[screenID];
			if (tempRect.width < tempRect.height) {
				if (!portrait) skip = true;
			} else {
				if (!landscape) skip = true;
			}
			if (!printSize) {
				if ( screenNames[screenID].IndexOf("Print") != -1 ) skip = true;
			}

			if (skip) {
				NextScreen();
			} else {
				// adjust to make iPad & 5.5 inch bigger
				if (screenNames[screenID].IndexOf("iPad") != -1 || screenNames[screenID].IndexOf("5-5") != -1) {
					pixelDensity = 2;
				} else if (screenNames[screenID].IndexOf("Print") != -1) {
					pixelDensity = 4;
				} else {
					pixelDensity = 1;
				}
				fileName = "ScreenShooter/" + screenNames[screenID] + id + ".png";
				GetMainGameView().position = new Rect(tempRect.x, tempRect.y, tempRect.width, tempRect.height+17);
				state = "Resizing";
				changeTime = Time.realtimeSinceStartup + pauseTime;
			}
			break;
		case "Resizing":
			if (Time.realtimeSinceStartup > changeTime) {
				changeTime = Time.realtimeSinceStartup + pauseTime;
				state = "Capturing";
				Application.CaptureScreenshot(fileName, pixelDensity);
			}
			break;
		case "Capturing":
			if (Time.realtimeSinceStartup > changeTime) {
				NextScreen();
			}
			break;
		case "CheckDrawCalls":
			CheckDrawCalls();
			break;
		default:
			if (Input.GetKey(KeyCode.G) && ( Input.GetKey(KeyCode.LeftCommand ) || Input.GetKey(KeyCode.RightCommand ) )  ) {
				StartCapture();
			}
			break;
		}
	}
	

	public static EditorWindow GetMainGameView() {
		System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
		System.Reflection.MethodInfo GetMainGameView = T.GetMethod("GetMainGameView",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
		System.Object Res = GetMainGameView.Invoke(null,null);
		return (EditorWindow)Res;
	}
	
	
}
